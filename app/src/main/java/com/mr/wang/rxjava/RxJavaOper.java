package com.mr.wang.rxjava;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;

/**
 * Rxjava操作符
 * User: chengwangyong(chengwangyong@vcinema.com)
 * Date: 2015-08-18
 * Time: 10:22
 */
public class RxJavaOper {
    /**
     * 处理集合数据1 基本处理
     */
    public void test1(){
        query("Hello, world!")
                .subscribe(urls -> {
                    for (String url : urls) {
                        System.out.println(url);
                    }
                });
    }

    /**
     * 还有Observable.from()方法，它接收一个集合作为输入，然后每次输出一个元素给subscriber：
     * flatMap()是不是看起来很奇怪？为什么它要返回另外一个Observable呢？理解flatMap的关键点在于，
     * flatMap输出的新的Observable正是我们在Subscriber想要接收的。
     * 现在Subscriber不再收到List<String>，
     * 而是收到一些列单个的字符串，就像Observable.from()的输出一样。
     */
    public void test2(){
//        query("Hello, world!")
//                .subscribe(urls -> {
//                    Observable.from(urls)
//                            .subscribe(url -> System.out.println(url));
//                });
        query("Hello").subscribe(urls->{
            Observable.from(urls).subscribe(System.out::println);
        });
    }

    /**
     * Observable.flatMap()接收一个Observable的输出作为输入，同时输出另外一个Observable。直接看代码：
     */
    public void test3(){
        query("Hello").flatMap(Observable::from).subscribe(System.out::println);
    }

    /**
     * flatMap进阶
     */
    public void test4(){
        query("Hello").flatMap(Observable::from).flatMap(this::getTitle).subscribe(System.out::println);
    }

    /**
     * 更加丰富的操作符 过滤null
     */
    public void test5(){
        query("Hello").flatMap(Observable::from).filter(url -> url != null).flatMap(this::getTitle).subscribe(System.out::println);
    }

    /**
     * 更加丰富的操作符 过滤null 取前三个take
     */
    public void test6(){
        query("Hello").flatMap(Observable::from).take(3).filter(url -> url != null).flatMap(this::getTitle).subscribe(System.out::println);
    }


    /**
     * 更加丰富的操作符 过滤null 取前三个take 然后保存起来
     * oOnNext()允许我们在每次输出一个元素之前做一些额外的事情，比如这里的保存标题。
     */
    public void test7(){
        query("Hello").flatMap(Observable::from).take(3).filter(url -> url != null).flatMap(this::getTitle).doOnNext(this::saveTitle).subscribe(System.out::println);
    }





    private void saveTitle(String s) {
        Log.i("TAG","保存"+s);
    }


    private Observable<String> getTitle(String url) {
        String substring = url.substring(5,url.length());
        return Observable.create(subscriber -> subscriber.onNext(substring));
    }


    private Observable<List<String>> query(String s) {
        ArrayList<String> list = new ArrayList<>();
        list.add("Hello Android");
        list.add("Hello Java");
        list.add("Hello RxJava");
        list.add("Hello Mr.wang");
        list.add(null);
        return Observable.create(subscriber -> subscriber.onNext(list));
    }
}  
