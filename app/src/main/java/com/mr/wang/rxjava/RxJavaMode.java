package com.mr.wang.rxjava;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * RxJava响应式编程
 * User: chengwangyong(chengwangyong@vcinema.com)
 * Date: 2015-08-18
 * Time: 11:00
 */
public class RxJavaMode {
    /**
     * onComplete()和onError()函数。这两个函数用来通知订阅者，被观察的对象将停止发送数据以及为什么停止（成功的完成或者出错了）。
     * <p>
     * 我觉得这种错误处理方式比传统的错误处理更简单。传统的错误处理中，通常是在每个回调中处理错误。
     * 这不仅导致了重复的代码，并且意味着每个回调都必须知道如何处理错误，你的回调代码将和调用者紧耦合在一起。
     */
    public void test1() {
        query("Hello").flatMap(Observable::from).flatMap(this::getTitle)
                .subscribe(new Subscriber<String>() {
                    @Override
                    public void onNext(String s) {
                        System.out.println(s);
                    }

                    @Override
                    public void onCompleted() {
                        System.out.println("Completed!");
                    }

                    @Override
                    public void onError(Throwable e) {
                        System.out.println("onError!");
                    }
                });
    }

    /**
     * android 可以用到的 指定运行的线程
     * <p>
     * subscribeOn()指定观察者代码运行的线程，使用observerOn()指定事件源运行的线程：
     */
    public void test2() {
        Subscription hello = query("Hello")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(System.out::println);
    }


    /**
     * 给每一数据源（网络、磁盘和内存）一个Observable<Data>接口，
     * 我们可以通过两个操作：concat()和first()，来实现一个简单的解决方案。
     * <p>
     * concat()持有多个Observables，并且把它们连接在队列里。
     * first()仅从队列里中获取到第一个条目。因此，
     * 如果你使用concat().first()可以从多个数据源中获取到第一个。
     */
    public void test3() {
        // Our sources (left as an exercise for the reader)
        Observable<String> memory = null;
        Observable<String> disk = null;
        Observable<String> network = null;

        // Retrieve the first source with data
        Observable<String> source = Observable
                .concat(memory, disk, network)
                .first();
    }


    private Observable<String> getTitle(String url) {
        String substring = url.substring(5, url.length());
        return Observable.create(subscriber -> subscriber.onNext(substring));
    }

    private Observable<List<String>> query(String s) {
        ArrayList<String> list = new ArrayList<>();
        list.add("Hello Android");
        list.add("Hello Java");
        list.add("Hello RxJava");
        list.add("Hello Mr.wang");
        list.add(null);
        return Observable.create(subscriber -> subscriber.onNext(list));
    }
}  
