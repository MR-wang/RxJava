package com.mr.wang.rxjava;

import rx.Observable;
import rx.Subscriber;
import rx.functions.Action1;
import rx.functions.Func1;

/**
 * RxJava模式浅析
 * User: chengwangyong(chengwangyong@vcinema.com)
 * Date: 2015-08-17
 * Time: 18:01
 */
public class RxJava {
    /**
     * 最初的写法
     */
    public void rxJava() {
        //创建一个被观察者/事件源
        Observable<String> myObservable  = Observable.create(new Observable.OnSubscribe<String>() {
            @Override
            public void call(Subscriber<? super String> subscriber) {
                subscriber.onNext("hello RxJava");
                subscriber.onCompleted();
            }
        });
        //创建一个观察者
        Subscriber<String> subscriber = new Subscriber<String>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(String s) {
                System.out.println("结果=="+s);
            }
        };
        //观察者订阅事件源
        myObservable.subscribe(subscriber);
    }

    /**
     * 更加简洁的代码
     */
    public void rxJavaSimple(){
//        Observable<String> myObservable = Observable.just("你好 RxJava");
//
//        Action1<String> action1 = new Action1<String>() {
//            @Override
//            public void call(String s) {
//                System.out.println(s);
//            }
//        };
//
//        myObservable.subscribe(action1);

        //更简化的版本
        Observable.just("你好，RxJava").subscribe(new Action1<String>() {
            @Override
            public void call(String s) {
                System.out.println(s);
            }
        });
    }

    /**
     * 操作符的使用 用于中途改变数据
     */
    public void Operators(){
        Observable.just("Hello Android").map(new Func1<String, String>() {
            @Override
            public String call(String s) {
                return s+" 加入的内容";
            }
        }).subscribe(new Action1<String>() {
            @Override
            public void call(String s) {
                System.out.println(s);
            }
        });
    }

    /**
     * Map操作符进阶 可以更改数据类型
     */
    public void Operators2(){
//        Observable.just("Hello Android ").map(new Func1<String, Integer>() {
//            @Override
//            public Integer call(String s) {
//                return s.hashCode();
//            }
//        }).subscribe(new Action1<Integer>() {
//            @Override
//            public void call(Integer s) {
//                System.out.println("RxJava响应="+s);
//            }
//        });
        Observable.just("Hello, world!")
                .map(s -> s + " -Dan")
                .subscribe(System.out::println);
    }

    /**
     * map操作符进阶3
     */
    public void Operators3(){
        //Observable.just("Hello Andord").map(String::hashCode).subscribe(i->System.out.println(Integer.toString(i)));
        Observable.just("Hello RxJava").map(String::hashCode).map(i->Integer.toString(i)).subscribe(System.out::println);
    }






}  
